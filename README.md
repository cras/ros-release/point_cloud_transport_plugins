## point_cloud_transport_plugins (noetic) - 1.0.5-1

The packages in the `point_cloud_transport_plugins` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic point_cloud_transport_plugins` on `Fri, 16 Jun 2023 14:08:08 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport_plugins.git
- rosdistro version: `1.0.3-1`
- old version: `1.0.4-1`
- new version: `1.0.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (melodic) - 1.0.5-1

The packages in the `point_cloud_transport_plugins` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport_plugins` on `Fri, 16 Jun 2023 14:06:06 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport_plugins.git
- rosdistro version: `1.0.3-1`
- old version: `1.0.4-1`
- new version: `1.0.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (noetic) - 1.0.4-1

The packages in the `point_cloud_transport_plugins` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic point_cloud_transport_plugins` on `Fri, 16 Jun 2023 13:59:49 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport_plugins.git
- rosdistro version: `1.0.3-1`
- old version: `1.0.3-1`
- new version: `1.0.4-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (melodic) - 1.0.4-1

The packages in the `point_cloud_transport_plugins` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport_plugins` on `Fri, 16 Jun 2023 13:52:55 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport_plugins.git
- rosdistro version: `1.0.3-1`
- old version: `1.0.3-1`
- new version: `1.0.4-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (noetic) - 1.0.3-1

The packages in the `point_cloud_transport_plugins` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track point_cloud_transport_plugins` on `Thu, 25 May 2023 18:39:24 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (melodic) - 1.0.3-1

The packages in the `point_cloud_transport_plugins` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport_plugins` on `Sat, 13 May 2023 01:38:21 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.2-1`
- new version: `1.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (melodic) - 1.0.2-1

The packages in the `point_cloud_transport_plugins` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport_plugins` on `Thu, 11 May 2023 14:02:58 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.1-1`
- new version: `1.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport_plugins (melodic) - 1.0.1-1

The packages in the `point_cloud_transport_plugins` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic --track melodic --new-track point_cloud_transport_plugins` on `Thu, 11 May 2023 03:47:25 -0000`

These packages were released:
- `draco_point_cloud_transport`
- `point_cloud_transport_plugins`

Version of package(s) in repository `point_cloud_transport_plugins`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport_plugins.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


